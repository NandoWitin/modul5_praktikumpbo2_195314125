package modul3A;

import javax.swing.JFrame;

public class Latihan1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ini Frame Pertamaku");

        int tinggi = 400;
        int lebar = 500;

        frame.setBounds(100, 100, lebar, tinggi);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
// PROGRAM LATIHAN 1 MODUL 3A
