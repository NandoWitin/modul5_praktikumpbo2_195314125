package modul3A;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class FrameKu extends JFrame {

    public FrameKu() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);

        JLabel label = new JLabel("Ini Label");
        panel.add(label);

        JTextField textField = new JTextField("Ini TextField");
        panel.add(textField);

        JCheckBox cBox = new JCheckBox("Ini Check Box");
        panel.add(cBox);

        JRadioButton radioButton = new JRadioButton("Ini Radio Button");
        panel.add(radioButton);

        this.add(panel);
    }

    public static void main(String[] args) {
        new FrameKu();
    }
}

// PROGRAM LATIHAN 2 DAN 3 MODUL 3A
