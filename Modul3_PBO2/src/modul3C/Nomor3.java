package modul3C;

import java.net.URL;
import javax.swing.*;

public class Nomor3 extends JDialog {

    private JLabel labelGambar;
    private JLabel labelText;

    public Nomor3() {
        this.setLayout(null);
        setTitle("Text and Icon Label");
        setSize(500, 400);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);

        URL loc = this.getClass().getResource("Bob.png");
        ImageIcon imageBob = new ImageIcon(loc);
        labelGambar = new JLabel();
        labelGambar.setIcon(imageBob);
        labelGambar.setBounds(135, 50, 200, 230);
        this.add(labelGambar);

        labelText = new JLabel("The Legend Of Reggae");
        labelText.setBounds(170, 290, 200, 30);
        this.add(labelText);
    }

    public static void main(String[] args) {
        new Nomor3();
    }
}
