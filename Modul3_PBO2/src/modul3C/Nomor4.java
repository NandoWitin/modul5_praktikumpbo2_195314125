package modul3C;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;

public class Nomor4 extends JDialog {

    private JTextArea textArea;
    private JCheckBox cBoxCentered, cBoxBold, cBoxItalic;
    private JButton buttonLeft, buttonRight;

    public Nomor4() {
        setLayout(null);
        setTitle("CheckBoxDemo");
        setSize(430, 200);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);

        textArea = new JTextArea();
        textArea.setText("Welcome to Java");
        textArea.setBounds(1, 1, 325, 95);
        textArea.setEditable(false);
        this.add(textArea);

        buttonLeft = new JButton("Left");
        buttonLeft.setBounds(130, 110, 70, 25);
        this.add(buttonLeft);
        buttonRight = new JButton("Right");
        buttonRight.setBounds(210, 110, 70, 25);
        this.add(buttonRight);

        cBoxCentered = new JCheckBox("Centered");
        cBoxCentered.setBounds(330, 1, 100, 30);
        this.add(cBoxCentered);
        cBoxBold = new JCheckBox("Bold");
        cBoxBold.setBounds(330, 30, 100, 30);
        this.add(cBoxBold);
        cBoxItalic = new JCheckBox("Italic");
        cBoxItalic.setBounds(330, 60, 100, 30);
        this.add(cBoxItalic);

    }

    public static void main(String[] args) {
        new Nomor4();
    }
}
