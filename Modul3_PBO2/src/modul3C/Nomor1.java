package modul3C;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Nomor1 extends JFrame {

    private final JMenuBar menuBar;
    private final JMenu menuFile, menuEdit;
    private final JMenuItem menuItem_tampil1, menuItem_tampil2;

    public Nomor1() {
        setTitle("Frame Pertama");
        setSize(400, 200);
        getContentPane().setBackground(Color.pink);

        menuBar = new JMenuBar();
        menuFile = new JMenu("File");
        menuEdit = new JMenu("Edit");
        menuItem_tampil1 = new JMenuItem("Tampil 1");
        menuItem_tampil2 = new JMenuItem("Tampil 2");
        menuFile.add(menuItem_tampil1);
        menuFile.add(menuItem_tampil2);
        menuBar.add(menuFile);
        menuBar.add(menuEdit);
        this.setJMenuBar(menuBar);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Nomor1();
    }
}
