package praktikum;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Kalkulator extends JFrame implements ActionListener {
    
    private JTextField textField_bil1, textField_bil2, textField_hasil;
    private JLabel label_bil1, label_bil2, label_hasil;
    private JButton button_jumlah;
    
    public Kalkulator() {
        setLayout(null);
        setTitle("Input Data");
        setSize(300, 280);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        
        label_bil1 = new JLabel("Bilangan 1");
        label_bil1.setBounds(20, 30, 80, 30);
        this.add(label_bil1);
        
        label_bil2 = new JLabel("Bilangan 2");
        label_bil2.setBounds(20, 80, 80, 30);
        this.add(label_bil2);
        
        label_hasil = new JLabel("Hasil");
        label_hasil.setBounds(20, 130, 80, 30);
        this.add(label_hasil);
        
        textField_bil1 = new JTextField();
        textField_bil1.setBounds(120, 30, 130, 30);
        this.add(textField_bil1);
        
        textField_bil2 = new JTextField();
        textField_bil2.setBounds(120, 80, 130, 30);
        this.add(textField_bil2);
        
        textField_hasil = new JTextField();
        textField_hasil.setBounds(120, 130, 130, 30);
        textField_hasil.setEditable(false);
        this.add(textField_hasil);
        
        button_jumlah = new JButton("Jumlah");
        button_jumlah.setBounds(120, 180, 80, 30);
        this.add(button_jumlah);
        
        button_jumlah.addActionListener(this);
    }
    
    public static void main(String[] args) {
        new Kalkulator();
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        double b1, b2, hasil;
        b1 = Double.parseDouble(textField_bil1.getText());
        b2 = Double.parseDouble(textField_bil2.getText());
        hasil = b1 + b2;
        textField_hasil.setText(Double.toString(hasil));
        
    }
}
