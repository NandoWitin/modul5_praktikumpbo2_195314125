package praktikum;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Tugas1 extends JFrame implements ActionListener {

    private JLabel label_panjang, label_lebar, label_hasil;
    private JTextField textField_panjang, textField_lebar, textField_hasil;
    private JButton button_hitung;

    public static void main(String[] args) {
        new Tugas1();
    }

    public Tugas1() {
        setTitle("Luas Tanah");
        setSize(280, 180);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(null);

        label_panjang = new JLabel("Panjang (m)");
        label_panjang.setBounds(10, 5, 80, 25);
        this.add(label_panjang);
        label_lebar = new JLabel("Lebar (m)");
        label_lebar.setBounds(10, 32, 80, 25);
        this.add(label_lebar);
        label_hasil = new JLabel("Luas (m2)");
        label_hasil.setBounds(10, 59, 80, 25);
        this.add(label_hasil);

        textField_panjang = new JTextField();
        textField_panjang.setBounds(95, 5, 150, 25);
        this.add(textField_panjang);
        textField_lebar = new JTextField();
        textField_lebar.setBounds(95, 32, 150, 25);
        this.add(textField_lebar);
        textField_hasil = new JTextField();
        textField_hasil.setEditable(false);
        textField_hasil.setBounds(95, 59, 150, 25);
        this.add(textField_hasil);

        button_hitung = new JButton("Hitung");
        button_hitung.setBounds(95, 90, 80, 25);
        this.add(button_hitung);

        button_hitung.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        double p, l, luas;
        try {
            p = Double.parseDouble(textField_panjang.getText());
            l = Double.parseDouble(textField_lebar.getText());
            luas = p * l;
            textField_hasil.setText(Double.toString(luas));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Maaf, hanya integer yang diperbolehkan!",
                    "ERROR", JOptionPane.ERROR_MESSAGE);

        }
    }

}
